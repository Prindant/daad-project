import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class DijkstraMain {
    HashMap<String,Vertex> vertices = new HashMap<>();

    public void readFromFile(String fileName) throws IOException {
        File file = new File(System.getProperty("user.dir") + "\\Lines\\" + fileName);

        ArrayList<Vertex> vx = new ArrayList<>();

        BufferedReader br = new BufferedReader(new FileReader(file));

        String st;
        while ((st = br.readLine()) != null)
            vx.add(new Vertex(st));
        for (int i = 0; i < vx.size(); i++) {
            if(i != vx.size() - 1)
                vx.get(i).addNeighbour(new Edge(1, vx.get(i), vx.get(i+1)));
        }
        for (int i = vx.size() - 1; i >= 0; i--) {
            if(i != 0)
                vx.get(i).addNeighbour(new Edge(1, vx.get(i), vx.get(i-1)));
        }
        for (int i = 0; i < vx.size(); i++) {
            if(vertices.containsKey(vx.get(i).getName())) {
                for(Edge e : vx.get(i).getAdjacenciesList()) {
                    vertices.get(vx.get(i).getName()).addNeighbour(e);
                }
            } else {
                vertices.put(vx.get(i).getName(),vx.get(i));
            }
        }
    }

    public static void main(String[] args) throws IOException {
        DijkstraMain dijkstraMain = new DijkstraMain();
        dijkstraMain.readFromFile("Chilonzor Line.txt");
        dijkstraMain.readFromFile("Uzbekistan Line.txt");
        dijkstraMain.readFromFile("Yunusobod Line.txt");

        dijkstraMain.test();
    }

    public void test() {
        DijkstraShortestPath shortestPath = new DijkstraShortestPath();
        vertices = shortestPath.computeShortestPaths(vertices, vertices.get("Minor")); // FROM
        System.out.println("=====================	=================");
        System.out.println("Calculating Path");
        System.out.println("======================================");

        System.out.println("Shortest Path from Minor to Kosmonavtlar: "+
                shortestPath.getShortestPathTo(vertices.get("Kosmonavtlar"))); // DESTINATION

    }
}